import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;
import org.junit.Test;

import com.owl.interview.tfl.ArrivalPrediction;
import com.owl.interview.tfl.TflService;

public class TflServiceTest {
	TflService tflService = new TflService();
	
	@Test
	public void testSortingOfList() throws Exception {
		String searchUrl = "https://api.tfl.gov.uk/Line/bakerloo/Arrivals/940GZZLUWLO";
		URL obj = new URL(searchUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String results = response.toString();
		// would be better to have fixed JSON. Using api for now to gain time
		
		List<JSONObject> jsonValues = tflService.sortResultsListByArrivalTime(results);
		for (int i = 0; i < jsonValues.size() - 1 ; i++) {
			assertTrue(jsonValues.get(i).getInt("timeToStation") <= jsonValues.get(i+1).getInt("timeToStation"));
		}
	}
	
	@Test
	public void testGenerateTextFromArrivalPrediction() {
		ArrivalPrediction arrivalPrediction = new ArrivalPrediction();
		Date date = new Date();
		arrivalPrediction.setLine("Victoria");
		arrivalPrediction.setDirection("Northbound");
		arrivalPrediction.setPlatform("Platform 1");
		arrivalPrediction.setDestination("Walthamstow Central");
		arrivalPrediction.setArrivalDate(date);
		String text = tflService.generateTextFromArrivalPrediction(arrivalPrediction);
		assertEquals(text, "Victoria line. Direction Walthamstow Central on Platform 1 arriving in 0 minutes.");
		assertEquals(tflService.sendArrival(text), "The next train is: Victoria line. Direction Walthamstow Central on Platform 1 arriving in 0 minutes.");
	}

}
