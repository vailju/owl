package com.owl.interview;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.owl.interview.tfl.ArrivalPrediction;
import com.owl.interview.tfl.StopInformation;
import com.owl.interview.tfl.TflService;

public class Main {
	
	public static void main (String[] args) throws Exception {
		TflService tflService = new TflService();
		StopInformation stopInformation = new StopInformation("940GZZLUWLO", "bakerloo");
		List<ArrivalPrediction> arrivalPredictions = tflService.getArrivalInfo(stopInformation);
		List<String> textsToRead = getListsOfTexts(tflService, arrivalPredictions);
		while (textsToRead.size() > 0) {
			textsToRead = getListsOfTexts(tflService, arrivalPredictions);
			if (textsToRead.size() == 0) {	
				System.out.println("Service interrupted");
			}
			if (textsToRead.size() > 0) {
				System.out.println(tflService.sendArrival(textsToRead.get(0)));
			}
			if (textsToRead.size() > 1) {
				System.out.println(tflService.sendFollowingArrivals(textsToRead.subList(1, textsToRead.size())));
			}
			TimeUnit.SECONDS.sleep(60); //called every 60 seconds for refresh
		}
				
	}

	private static List<String> getListsOfTexts(TflService tflService, List<ArrivalPrediction> arrivalPredictions) {
		List<String> textsToRead = new ArrayList<String>();
		for (ArrivalPrediction arrivalPrediction : arrivalPredictions) {
			String text = tflService.generateTextFromArrivalPrediction(arrivalPrediction);
			if (!text.equals("arrived")) { //train arrived, not on BOARD
				textsToRead.add(tflService.generateTextFromArrivalPrediction(arrivalPrediction));
			}
		}
		return textsToRead;
	}

}
