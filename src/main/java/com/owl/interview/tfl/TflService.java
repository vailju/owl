package com.owl.interview.tfl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TflService {
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'H:m:s");
	int maxTrainsToDisplay = 3; //easier to change if we decide to change number of trains to display
	
	
	public List<ArrivalPrediction> getArrivalInfo(StopInformation stopInformation) {

		//url to get to the TFL feed
		String baseUrl = "https://api.tfl.gov.uk/Line/%s/Arrivals/%s";
		String searchUrl = String.format(baseUrl, stopInformation.getLineId(), stopInformation.getStopPointId());
		List<ArrivalPrediction> arrivalPredictions = new ArrayList<ArrivalPrediction>();
		try {
			String results = getApiResponse(searchUrl);
			List<JSONObject> jsonValues = sortResultsListByArrivalTime(results); //feed is not ordered, we need to sort to get next trains
			for (int i = 0 ; i < Math.min(maxTrainsToDisplay, jsonValues.size()) ; i++) { // check min in case not enough trains left
				JSONObject result = jsonValues.get(i);
				ArrivalPrediction arrivalPrediction = extractArrivalPrediction(result);
				arrivalPredictions.add(arrivalPrediction);		
			}
			
		} catch (Exception e) {
			System.out.println("Error retrieving feed: " + e.getMessage());
		}
		return arrivalPredictions;

	}
	
	public String sendArrival(String text) {
		return "The next train is: " + text;
	}
	
	public String sendFollowingArrivals(List<String> textsList) {
		return "The following trains are: " + textsList.toString();
	}

	private String getApiResponse(String searchUrl) throws MalformedURLException, IOException, ProtocolException {
		URL obj = new URL(searchUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String results = response.toString();
		return results;
	}
	
	public List<JSONObject> sortResultsListByArrivalTime(String results) {
		JSONArray jsonResult = new JSONArray(results);
		List<JSONObject> jsonValues = new ArrayList<JSONObject>();
		for (int i = 0; i < jsonResult.length(); i++) {
			jsonValues.add(jsonResult.getJSONObject(i));
		}
		Collections.sort(jsonValues, new Comparator<JSONObject>() {
			private static final String KEY_NAME = "timeToStation"; //timeToStation is integer giving seconds to arrival

			public int compare(JSONObject a, JSONObject b) {
				int valA = 0;
				int valB= 0;

				try {
					valA = a.getInt(KEY_NAME);
					valB =  b.getInt(KEY_NAME);
				} 
				catch (JSONException e) {
				}

				return valA-valB;

			}
		});
		return jsonValues;
	}

	private ArrivalPrediction extractArrivalPrediction(JSONObject result) throws ParseException {
		ArrivalPrediction arrivalPrediction = new ArrivalPrediction();
		//extracts information from obtained API
		//TODO: extract those dependencies in a map or config file
		arrivalPrediction.setLine(result.getString("lineName"));
		arrivalPrediction.setDirection(result.getString("direction"));
		arrivalPrediction.setPlatform(result.getString("platformName"));
		arrivalPrediction.setDestination(result.getString("destinationName"));
		arrivalPrediction.setArrivalDate(formatter.parse(result.getString("expectedArrival")));

		return arrivalPrediction;
	}
	
	public String generateTextFromArrivalPrediction(ArrivalPrediction arrivalPrediction) {
		int minutes = getMinutesToWait(arrivalPrediction.getArrivalDate());
		if (minutes < 0) {
			return "arrived";//could be used if want to refresh without API call
		}
		return arrivalPrediction.getLine() + " line. Direction " + arrivalPrediction.getDestination() + " on " +  arrivalPrediction.getPlatform() 
		+ " arriving in " + getMinutesToWait(arrivalPrediction.getArrivalDate()) + " minutes.";
	}
	
	private int getMinutesToWait(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.setTime(new Date());
		// gets the difference of minutes between 2 times
		if (calendar.get(Calendar.HOUR_OF_DAY) == currentCalendar.get(Calendar.HOUR_OF_DAY)) {
			return calendar.get(Calendar.MINUTE) - currentCalendar.get(Calendar.MINUTE);
		}
		else {
			return (60 - currentCalendar.get(Calendar.MINUTE) + calendar.get(Calendar.MINUTE));
		}
	}
	
}
