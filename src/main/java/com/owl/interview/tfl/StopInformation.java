package com.owl.interview.tfl;

public class StopInformation {
	private String stopPointId;
	private String lineId;
	
	public StopInformation(String stopPointId, String lineId) {
		this.stopPointId = stopPointId;
		this.lineId = lineId;
	}
	public String getStopPointId() {
		return stopPointId;
	}
	public void setStopPointId(String stopPointId) {
		this.stopPointId = stopPointId;
	}
	public String getLineId() {
		return lineId;
	}
	public void setLineId(String lineId) {
		this.lineId = lineId;
	}
	
}
